import logging

import requests

from config import REQUEST_TIMEOUT
from exceptions import UnableToGetProfileJSONException
from utils import set_query_parameter, check_key_exists


def make_request(url, proxies):
    result = requests.get(url, proxies=proxies, verify=False, timeout=REQUEST_TIMEOUT)

    # result = requests.get(url, proxies=proxies, verify=False, timeout=REQUEST_TIMEOUT)
    # result = requests.get(_url, verify=False, timeout=REQUEST_TIMEOUT)

    if 200 <= result.status_code <= 300:
        return result.json()
    else:
        print('Profile: {} respond with status: {}'.format(url, result.status_code))
        return {}


def get_json_profile(url, proxy):
    proxies = {
        'https': proxy,
        'http': proxy,
    }

    _url = set_query_parameter(url, '__a', 1)

    print('Getting profile json: {profile}'.format(profile=_url))

    try:
        return make_request(_url, proxies)
    except ValueError as e:
        with open('valid.txt', 'a') as f:
            f.write('{proxy}\n'.format(proxy=proxy))

        logging.error('Invalid JSON response from URL: {}'.format(_url))

        raise UnableToGetProfileJSONException()


def parse_external_link(url, proxy_pool):
    success = False

    while not success:
        proxy = proxy_pool.next()
        print('Using proxy for request: {proxy}'.format(proxy=proxy))

        try:
            profile_json = get_json_profile(url, proxy)
            success = True

            if check_key_exists(profile_json, 'graphql', 'user', 'external_url'):
                return profile_json['graphql']['user']['external_url']
            else:
                return None
        except Exception as e:
            logging.error(str(e))

            success = False

            proxy_pool.ban_current()
