import re
from urllib.parse import urlencode, parse_qs, urlsplit, urlunsplit

import requests
from lxml.html import fromstring


def set_query_parameter(url, param_name, param_value):
    scheme, netloc, path, query_string, fragment = urlsplit(url.strip())
    query_params = parse_qs(query_string)

    query_params[param_name] = [param_value]
    _query_string = urlencode(query_params, doseq=True)

    return urlunsplit((scheme, netloc, path, _query_string, fragment))


def check_key_exists(obj, *keys):
    o = obj

    for key in keys:
        if key not in o:
            return False

        o = o[key]

    return True


def get_proxies():
    url = 'https://free-proxy-list.net/'
    response = requests.get(url)
    parser = fromstring(response.text)
    proxies = set()

    for i in parser.xpath('//tbody/tr')[:10]:
        if i.xpath('.//td[7][contains(text(),"yes")]'):
            proxy = ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
            proxies.add(proxy)

    return proxies


def is_proxy(str):
    return re.match(
        r'^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]):[0-9]{1,5}$',
        str)
