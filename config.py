import warnings
warnings.filterwarnings("ignore")


REQUEST_TIMEOUT = 5

# request limit for each proxy
PROXY_REQUEST_LIMIT = 1

# sleep in seconds after reaching end of proxy list
PROXY_LIST_END_SLEEP = 5

# Ban for proxy in seconds
PROXY_BAN_TTL = 60