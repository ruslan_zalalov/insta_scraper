import argparse
import csv
import os
import asyncio

import validators

from proxy_pool import ProxyPool
from scraper import parse_external_link


def profiles_pool(path):
    if not os.path.exists(path):
        raise FileNotFoundError('{path} not exists!'.format(path=path))

    with open(path, 'r') as f:
        for profile in f:
            yield profile


def main(profiles_file, proxies_file):
    proxy_pool = ProxyPool(proxies_file)

    with open('profile_urls.csv', mode='w') as urls_file:
        urls_writer = csv.writer(urls_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        for profile in profiles_pool(profiles_file):
            if not validators.url(profile):
                continue

            print('\n')
            print('-' * 10)

            url = profile.strip()
            link = parse_external_link(url, proxy_pool)

            if link:
                urls_writer.writerow([profile.strip(), link])
                urls_file.flush()

                print('\n!!! Profile {profile} has external link: {link}'.format(profile=url, link=link))

            print('-' * 10)
            print('\n')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Instagram Profiles External Urls Scraper.')
    parser.add_argument(
        '--profiles',
        help='profiles list filepath'
    )
    parser.add_argument(
        '--proxies',
        help='proxies list filepath'
    )

    args = parser.parse_args()

    main(args.profiles, args.proxies)
