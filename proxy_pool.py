import logging
import time
from collections import defaultdict
from datetime import datetime

from config import PROXY_REQUEST_LIMIT, PROXY_LIST_END_SLEEP, PROXY_BAN_TTL


class ProxyPool:
    def __init__(self, filepath):
        self.filepath = filepath
        self.request_counter = defaultdict(int)
        self.banned = {}
        self.current = None
        self.proxy_request_limit = PROXY_REQUEST_LIMIT

        self.__init_proxy_generator__()
        self.update_current()

    def __init_proxy_generator__(self):
        self.gen = self.get_proxy_generator()

    def get_proxy_generator(self):
        with open(self.filepath, 'r') as f:
            for line in f:
                yield self.parse_line(line)

    def parse_line(self, line):
        parts = line.strip().split(':')

        if len(parts) > 2:
            return 'http://{username}:{password}@{host}:{port}/'.format(
                username=parts[2],
                password=parts[3],
                host=parts[0],
                port=parts[1],
            )
        else:
            return 'http://{host}:{port}/'.format(
                host=parts[0],
                port=parts[1],
            )

    def add_request(self):
        self.request_counter[self.current] += 1

    def update_current(self):
        try:
            self.current = next(self.gen)
        except StopIteration:
            logging.warning(
                '!!! Proxy list ended. Getting back to the beginning of the list. Sleeping.....\n----------\n\n'
            )

            self.__init_proxy_generator__()

            time.sleep(PROXY_LIST_END_SLEEP)

    def ban_current(self):
        self.banned[self.current] = datetime.now()

        logging.warning('Banning proxy: {proxy}'.format(proxy=self.current))

    def check_if_banned(self):
        if self.current not in self.banned:
            return False

        return (datetime.now() - self.banned[self.current]).total_seconds() < PROXY_BAN_TTL

    def next(self):
        self.update_current()

        while self.check_if_banned():
            logging.warning('Proxy {proxy} is banned. Getting next one...'.format(proxy=self.current))

            self.update_current()

        # if self.request_counter[self.current] >= PROXY_REQUEST_LIMIT:
        #     self.update_current()

        self.add_request()

        return self.current

    # def decrease_proxy_request_limit(self):
    #     # Avoid setting request limit lower than 5
    #     if self.proxy_request_limit <= 9:
    #         return
    #
    #     self.proxy_request_limit -= 5
