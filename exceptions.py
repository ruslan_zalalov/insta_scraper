
class EndOfProxyList(Exception):
    def __init__(self):
        super(EndOfProxyList, self).__init__('Proxy list has finished.')

class UnableToGetProfileJSONException(Exception):
    def __init__(self):
        super(UnableToGetProfileJSONException, self).__init__('Unable to get profile JSON.')
